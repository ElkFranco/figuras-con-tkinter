from Punto import *
from math import *
from Figura import *

class Circulo(Figura):
        
   def calcularArea(self):
      self.area = pi * (self.p1.calcularDistancia(self.p2)**2)     
        
   def calcularPerimetro(self):
      self.perimetro = (2  * pi) * (self.p1.calcularDistancia(self.p2))  
