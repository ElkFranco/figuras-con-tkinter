from Punto import *
from Rectangulo import *
from Circulo import *
from Triangulo import *
from Cuadrado import *
import Tkinter as tk

class Aplicacion:
    def __init__(self):
        self.root=tk.Tk()
        self.root.geometry('600x600')
        self.p1 = Punto()
        self.p2 = Punto()
        self.seleccion=tk.IntVar()
        self.seleccion2=tk.IntVar()
        self.seleccion.set(1)
        self.seleccion2.set(5)

        self.radio1=tk.Radiobutton(self.root,text="Rectangulo", variable=self.seleccion, value=1)
        self.radio1.grid(column=0, row=0)
        self.radio2=tk.Radiobutton(self.root,text="Circulo", variable=self.seleccion, value=2)
        self.radio2.grid(column=0, row=1)
        self.radio3=tk.Radiobutton(self.root,text="Triangulo", variable=self.seleccion, value=3)
        self.radio3.grid(column=0, row=3)
        self.radio4=tk.Radiobutton(self.root,text="Cuadrado", variable=self.seleccion, value=4)
        self.radio4.grid(column=0, row=4)
        self.radio5=tk.Radiobutton(self.root,text="Area", variable=self.seleccion2, value=5)
        self.radio5.grid(column=0, row=5)
        self.radio6=tk.Radiobutton(self.root,text="Perimetro", variable=self.seleccion2, value=6)
        self.radio6.grid(column=0, row=6)
        self.boton1=tk.Button(self.root, text="Mostrar seleccionado", command=self.mostrarseleccionado)
        self.boton1.grid(column=0, row=8)
        self.label1=tk.Label(text="opcion seleccionada")
        self.label1.grid(column=0, row=9)

        self.etiqueta = tk.Label(text="Punto 1 x: ")
        self.etiqueta.grid(column=0, row=10)
        self.p1_x = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p1_x)
        self.campo_de_textox1.grid(column=0, row=11)

        self.etiqueta = tk.Label(text="Punto 1 y: ")
        self.etiqueta.grid(column=0, row=12)
        self.p1_y = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p1_y)
        self.campo_de_textox1.grid(column=0, row=13)

        self.etiqueta = tk.Label(text="Punto 2 x: ")
        self.etiqueta.grid(column=0, row=14)
        self.p2_x = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p2_x)
        self.campo_de_textox1.grid(column=0, row=15)

        self.etiqueta = tk.Label(text="Punto 2 y: ")
        self.etiqueta.grid(column=0, row=16)
        self.p2_y = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p2_y)
        self.campo_de_textox1.grid(column=0, row=17)
        
        self.root.mainloop()

    def mostrarseleccionado(self):
        self.p1 = Punto()
        self.p2 = Punto()
        self.p1.x = self.p1_x.get()
        self.p1.y = self.p1_y.get()
        self.p2.x = self.p2_x.get()
        self.p2.y = self.p2_y.get()
        
        if self.seleccion.get()==1 and self.seleccion2.get()==5:
            self.r = Rectangulo()
            self.r.setPuntos(self.p1, self.p2)
            self.r.calcularArea()
            #print "Area Rectangulo: " + str(self.r.area)
            self.label1.configure(text="Area Rectangulo: "+str(self.r.area))
        if self.seleccion.get()==2 and self.seleccion2.get()==5:
            self.c = Circulo()
            self.c.setPuntos(self.p1, self.p2)
            self.c.calcularArea()
            #print "Area Circulo: " + str(self.c.area)
            self.label1.configure(text="Area Circulo: "+str(self.c.area))
        if self.seleccion.get()==3 and self.seleccion2.get()==5:
            self.t = Triangulo()
            self.t.setPuntos(self.p1, self.p2)
            self.t.calcularArea()
            #print "Area Triangulo: " + str(self.t.area)
            self.label1.configure(text="Area Triangulo: "+str(self.t.area))
        if self.seleccion.get()==4 and self.seleccion2.get()==5:
            self.cua = Cuadrado()
            self.cua.setPuntos(self.p1, self.p2)
            self.cua.calcularArea()
            #print "Area Cuadrado: " + str(self.cua.area)
            self.label1.configure(text="Area Cuadrado: "+str(self.cua.area))
        if self.seleccion.get()==1 and self.seleccion2.get()==6:
            self.r = Rectangulo()
            self.r.setPuntos(self.p1, self.p2)
            self.r.calcularPerimetro()
            #print "Perimetro Rectangulo: " + str(self.r.perimetro)
            self.label1.configure(text="Perimetro Cuadrado: "+str(self.r.perimetro))
        if self.seleccion.get()==2 and self.seleccion2.get()==6:
            self.c = Circulo()
            self.c.setPuntos(self.p1, self.p2)
            self.c.calcularPerimetro()
            #print "Perimetro Circulo: " + str(self.c.perimetro)
            self.label1.configure(text="Perimetro Circulo: "+str(self.c.perimetro))
        if self.seleccion.get()==3 and self.seleccion2.get()==6:
            self.t = Triangulo()
            self.t.setPuntos(self.p1, self.p2)
            self.t.calcularPerimetro()
            #print "Perimetro Triangulo: " + str(self.t.perimetro)
            self.label1.configure(text="Perimetro Triangulo: "+str(self.t.perimetro))
        if self.seleccion.get()==4 and self.seleccion2.get()==6:
            self.cua = Cuadrado()
            self.cua.setPuntos(self.p1, self.p2)
            self.cua.calcularPerimetro()
            #print "Perimetro Cuadrado: " + str(self.cua.perimetro)
            self.label1.configure(text="Perimetro Cuadrado: "+str(self.cua.perimetro))

aplicacion1=Aplicacion()
